var fs = require('fs');
var http = require('http');
var url = require("url");
var Qs = require('qs');
var autodesc = require('./auto_long_desc.js') ;
/*
var http = require('http');
var url = require("url");
var Qs = require('qs');
var autodesc = require('./auto_long_desc.js') ;
var jsdom = require("jsdom"); 
$ = require("jquery")(jsdom.jsdom().defaultView); 
*/

var q = process.argv[2] ;
var mode = 'long' ; // 'short'
var lang = process.argv[3] || 'en' ;
var media = 1 ;
var thumb = 120 ;

if ( (q||'') != '' ) {
	autodesc.getDescription ( { q:q , mode:mode , links:'wikidata' , get_infobox:'no' , lang:lang=='any'?'en':lang } , function ( al ) {
		var use_lang = lang=='any'?undefined:lang ;
		var j = {
			q : q ,
			label : autodesc.wd.items[q].getLabel(use_lang) ,
			manual_description : autodesc.wd.items[q].getDesc(use_lang) ,
			result : al
		} ;
		
		function show () {
			console.log ( j ) ;
		}
		
		if ( media ) {
			autodesc.addMedia ( j , thumb , show ) ;
		} else show() ;

//		console.log ( lang + ': ' + autodesc.wd.items[q].getLabel(lang) + ' : ' + al )
	} ) ;


} else {

	for ( var cnt = 0 ; cnt < 10 ; cnt++ ) {

		$.getJSON ( 'http://www.wikidata.org/w/api.php?callback=?' , {
			format:'json',
			action:'query',
			list:'random',
			rnnamespace:0,
			rnlimit:10
		} , function ( d ) {
			$.each ( d.query.random , function ( k , v ) {
				var q = v.title ;
				autodesc.getDescription ( { q:q , mode:mode } , function ( al ) {
		//			console.log ( autodesc.wd.items[q].getLabel('en') + "\t" + $.trim(al) ) ;
					console.log ( q + "\t" + $.trim(al.replace(/\s+/g,' ')) ) ;
				} ) ;
			} ) ;
		} ) ;

	}

}

