var fs = require('fs');

var infobox_generator = {
	wd : {} ,
	infoboxes : [] ,
	ready : false ,
	no_infobox_string : '' , //'<!-- No infobox available -->' ,
	
	init : function () {
		var self = this ;
		if ( self.ready ) return ;
		var data = fs.readFileSync('infoboxes.json', 'utf8' ) ;
		self.infoboxes = JSON.parse ( data ) ;
		self.ready = true ;
	} ,
	
	ucfirst : function ( s ) {
		return s.charAt(0).toUpperCase() + s.slice(1);
	} ,
	
	findInfobox : function ( o ) {
		var self = this ;
		var wiki = (o.lang||'en') + 'wiki' ;
		var ret ;
		
		if ( typeof o.template != 'undefined' && o.template != '' ) {
			var template = self.ucfirst(o.template.replace(/_/g,' ')) ;
			$.each ( self.infoboxes , function ( k , v ) {
				if ( v.wiki != wiki ) return ;
				if ( self.ucfirst(v.template.replace(/_/g,' ')) != template ) return ;
				ret = v ;
				return false ;
			} ) ;
			return ret ;
		}
		
		// Try auto-detect based on conditions
		
		$.each ( self.infoboxes , function ( k , v ) {
			if ( v.wiki != wiki ) return ;
			$.each ( (v.conditions||{}) , function ( check_p , check_qs ) {
				var q_list = self.wd.items[o.q].getClaimItemsForProperty ( check_p , true ) ;
				$.each ( check_qs , function ( dummy , check_q ) {
					if ( 0 > $.inArray ( check_q , q_list ) ) return ;
					ret = v ;
					return false ;
				} ) ;
				if ( typeof ret != 'undefined' ) return false ;
			} ) ;
			if ( typeof ret != 'undefined' ) return false ;
		} ) ;
		
		return ret ;
	} ,
	
	getFilledInfobox : function ( options ) {
		var self = this ;
		var q = options.q ;
		var lang = options.lang || 'en' ;
		var callback = options.callback ;
		q = 'Q'+(q+'').replace(/\D/g,'')
		self.wd.getItemBatch ( [q] , function () {
			var ib = self.findInfobox ( options ) ;
			if ( typeof ib == 'undefined' ) return callback(self.no_infobox_string) ; // No matching infobox found, blank string returned
			
			var items2load = [] ;
			$.each ( ib.params , function ( dummy , param ) {
				if ( (param.value||'') == '' ) return ; // Dummy entries, ignore
				if ( param.value.match(/^P\d+/) ) {
					var claims = self.wd.items[q].raw.claims[param.value] ;
					if ( typeof claims == 'undefined' ) return ;
					$.each ( claims , function ( k , v ) {
						if ( typeof v.mainsnak == 'undefined' ) return ;
						if ( v.mainsnak.datatype != 'wikibase-item' ) return ;
						if ( typeof v.mainsnak.datavalue == 'undefined' ) return ;
						if ( typeof v.mainsnak.datavalue.value == 'undefined' ) return ;
						var q2 = v.mainsnak.datavalue.value['numeric-id'] ;
						items2load.push ( q2 ) ;
					} ) ;
				}
			} ) ;
			
			self.wd.getItemBatch ( items2load , function () {
			
				var rows = [] ;
				rows.push ( "{{"+ib.template ) ;

				$.each ( ib.params , function ( dummy , param ) {
					if ( (param.value||'') == '' ) return ; // Dummy entries, ignore
					var pre = param.pre||'' ;
					var post = param.post||'' ;
					var sep = param.sep||"; " ;
					if ( param.value == 'label' ) {
						rows.push ( "| " + param.name + " = " + pre + self.wd.items[q].getLabel() + post ) ;
					} else if ( param.value == 'alias' ) {
						var s = self.wd.items[q].getAliasesForLanguage ( lang , false ) ;
						var parts = [] ;
						$.each ( s , function ( k , v ) { parts.push ( pre + v + post ) } ) ;
						rows.push ( "| " + param.name + " = " + parts.join(sep) ) ;
					} else if ( param.value.match(/^P\d+/) ) {
						var claims = self.wd.items[q].raw.claims[param.value] || [] ;
						var parts = [] ;
						var cnt = 0 ;
						$.each ( claims , function ( k , v ) {
							if ( v.mainsnak.datatype == 'commonsMedia' ) {
								parts.push ( pre + v.mainsnak.datavalue.value + post ) ;
							} else if ( v.mainsnak.datatype == 'string' ) {
								parts.push ( pre + v.mainsnak.datavalue.value + post ) ;
							} else if ( v.mainsnak.datatype == 'url' ) {
								parts.push ( pre + v.mainsnak.datavalue.value + post ) ;
							} else if ( v.mainsnak.datatype == 'time' ) {
								var time = self.wd.items[q].getClaimDate(v) ;
								var precision = time.precision ;
								time = time.time ;
								var era = time.match(/^-/) ? " BCE" : "" ;
								if ( precision <= 9 ) {
									time = time.substr(8,4) ;
								} else if ( precision == 10 ) {
									time = time.substr(8,7) ;
								} else if ( precision == 11 ) {
									time = time.substr(8,10) ;
								}
								parts.push ( pre + time.replace(/^0+/,'') + era + post ) ;
							} else if ( v.mainsnak.datatype == 'wikibase-item' ) {
								var q2 = 'Q'+v.mainsnak.datavalue.value['numeric-id'] ;
								var wikitext = self.wd.items[q2].getLabel() ;
								var wiki = lang + 'wiki' ;
								var wl = self.wd.items[q2].getWikiLinks() ;
								if ( typeof wl[wiki] != 'undefined' ) {
									if ( self.ucfirst(wl[wiki].title) != self.ucfirst(wikitext) ) wikitext = '[[' + wl[wiki].title + '|' + wikitext + ']]' ;
									else wikitext = '[[' + wikitext + ']]' ;
								}
								parts.push ( pre + wikitext + post ) ;
							} else {
								parts.push ( pre + param + post ) ;
								console.log ( v ) ;
							}
							cnt++ ;
							if ( typeof param.max == 'undefined' ) return ;
							if ( cnt >= param.max ) return false ;
						} ) ;
						if ( param.minor && parts.length == 0 ) return ;
						rows.push ( "| " + param.name + " = " + parts.join(sep) ) ;
					} else {
						if ( param.minor ) return ;
						rows.push ( "| " + param.name + " = " ) ;
					}
				} ) ;
			
			
				rows.push ( "}}" ) ;
				callback ( rows.join("\n")+"\n" ) ;
			
			} ) ;
			
		} ) ;
	} ,
	
	end:true
	
} ;

exports.ig = infobox_generator ;
